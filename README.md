![Illustration of the CIGA algorithm](summary.png "Illustration of the CIGA algorithm")

# Reproducing the results
First, clone this repository and `cd` into it.

```bash
git clone https://gitlab.com/florisdf/acpr2019.git
cd acpr2019
```
## Download caches
Next, download the `cache` folder containing the detections and embeddings for the LFW dataset from [this link](https://kuleuven.box.com/s/32x66nnflhkth7v4h9oauojro4h42j9i)
and paste the folder into the **evaluation** folder.
The acpr2019/evaluation folder should now contain these files:
- the [cache](https://kuleuven.box.com/s/32x66nnflhkth7v4h9oauojro4h42j9i) directory you just downloaded
- [ciga.py](evaluation/ciga.py)
- [confs.py](evaluation/confs.py)
- [trial.ipynb](evaluation/trial.ipynb)
- [experiment.ipynb](evaluation/experiment.ipynb)

## Run the docker
Now pull and run the docker image, forwarding the port that we will use for the Jupyter Notebook web server.

```bash
docker run -it --rm -v <your dir of acpr2019 clone>/:/root/project -p8888:8888 florisdf/acpr2019
```

> **NOTE** If you want to do the detections yourself and/or extract the embeddings yourself - instead of using [our cached versions](https://kuleuven.box.com/s/32x66nnflhkth7v4h9oauojro4h42j9i) - you will need to run the docker image with [nvidia-docker](https://github.com/NVIDIA/nvidia-docker).
> You will also need to provide an extra flag to the `nvidia-docker` command that makes the [LFW data](http://vis-www.cs.umass.edu/lfw/lfw.tgz) available inside the docker: ` -v <your LFW dir>/:/root/project/data/LFW`

## Run the notebook
Inside the docker, run Jupyter Notebook, using the port you forwarded in the `docker run` command.

```bash
cd /root/project
jupyter notebook --no-browser --port=8888
```

This will print something like
```
[I 10:21:40.498 NotebookApp] Writing notebook server cookie secret to /root/.local/share/jupyter/runtime/notebook_cookie_secret
[I 10:21:40.724 NotebookApp] [jupyter_nbextensions_configurator] enabled 0.4.1
[I 10:21:40.725 NotebookApp] Serving notebooks from local directory: /root/project
[I 10:21:40.725 NotebookApp] The Jupyter Notebook is running at:
[I 10:21:40.725 NotebookApp] http://(167f4c2101dd or 127.0.0.1):8888/?token=<some long string of random characters>
[I 10:21:40.725 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 10:21:40.730 NotebookApp]

    To access the notebook, open this file in a browser:
        file:///root/.local/share/jupyter/runtime/nbserver-249-open.html
    Or copy and paste one of these URLs:
        http://(167f4c2101dd or 127.0.0.1):8888/?token=<some long string of random characters>
```

Copy the string of characters behind `?token=`. Now open your browser and go to `http://localhost:8888/?token=<the characters you copied>`.

Next, open the [experiment.ipynb](evaluation/experiment.ipynb) notebook. Running this will reproduce all the results obtained for the LFW dataset. This might take a while...

## Configuring the experiment
The [experiment.ipynb](evaluation/experiment.ipynb) works by running multiple configurations of [trial.ipynb](evaluation/trial.ipynb).
Which configurations [experiment.ipynb](evaluation/experiment.ipynb) uses, is determined by the `conf` dict in [confs.py](evaluation/confs.py).
By using a list as the value of a key in the second level of this dict, [experiment.ipynb](evaluation/experiment.ipynb) will
run [trial.ipynb](evaluation/trial.ipynb) with a configuration for each value in that list.

For example, `conf['data']['seed']` is set to `[*range(30)]`. Hence, [trial.ipynb](evaluation/trial.ipynb) will be repeated for
values of `conf['data']['seed']` ranging from 0 to 29. When multiple values are given as a list, the trial will be
run for each possible combination of these two lists. Check out the [confs.py](evaluation/confs.py) file for more information.

# Using CIGA
Our paper describes the CIGA algorithm, which we implemented in the [ciga.py](evaluation/ciga.py) file.
This can be easily used in any Python 3 project. Its only dependencies are [numpy](https://numpy.org/), [scipy](https://www.scipy.org/) and [pandas](https://pandas.pydata.org/).