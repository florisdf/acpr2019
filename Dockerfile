FROM florisdf/torch_flocker

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs

COPY environment.yml /root/environment.yml
RUN conda env create -f /root/environment.yml
RUN /opt/conda/envs/ipta2019/bin/python -m ipykernel install --user --name=ipta2019
RUN git clone https://gitlab.com/EAVISE/flutil.git /root/scripts/flutil
WORKDIR /root/scripts/flutil
RUN git reset --hard c3afec1ce52c916e7962b9dfeba55d3e99a4268a
WORKDIR /

ENV CONDA_DEFAULT_ENV acpr2019
