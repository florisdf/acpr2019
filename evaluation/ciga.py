import numpy as np
from scipy.spatial.distance import cdist
from numpy.ma import masked_array as ma
import pandas as pd


class CIGA:
    def __init__(self, df_gallery, df_query, clust):
        """
        :param df_gallery: a `DataFrame` with (labeled) gallery descriptors in
        a column named 'descriptor'. The column 'label' should contain the
        corresponding labels
        :param df_query: a `DataFrame` with (unlabeled) query descriptors in a
        column named 'descriptor'
        :param clust: an object from a class that implements the
        `sklearn.base.ClusterMixin` interface
        """
        self.df_gallery = df_gallery
        self.df_query = df_query
        self.clust = clust

    def apply(self):
        """Apply CIGA"""
        self.cluster()
        unknown_refs = self.get_unknown_refs()
        df_g_augmented = pd.concat([self.df_gallery,
                                    pd.DataFrame(unknown_refs)])
        return self.filter_duprefs(df_g_augmented)

    def cluster(self):
        """
        We cluster both the query faces and the known gallery faces. The
        clusters in which the gallery faces are put, are considered to be known
        clusters.  From each unkown cluster, a reference vector will be
        extracted by selecting the most central vector.
        """
        X_q = np.vstack(self.df_query['descriptor'].values)
        X_g = np.vstack(self.df_gallery['descriptor'].values)

        X = np.vstack([X_q, X_g])
        labels = self.clust.fit_predict(X)

        self.df_query['cluster'] = labels[:len(self.df_query)]
        self.df_gallery['cluster'] = labels[len(self.df_query):]

    def get_unknown_refs(self):
        """
        Find queries that belong to a different cluster than the clusters with
        the known reference images. These are probably people of whom we don't
        have a reference image.

        We try to pick a good reference image for each unkown cluster by
        selecting the image to which the elements of that cluster have, on
        average, the smallest distance.
        """

        # NOTE: c == -1 means "outlier"
        known_clusters = self.df_gallery['cluster']
        df_q_unkown = (self
                       .df_query[self.df_query['cluster']
                                 .apply(lambda c: (c not in known_clusters
                                                   and c != -1))])
        unkown_refs = []

        for cluster, group in df_q_unkown.groupby('cluster'):
            # Find vector that has, on average, the smallest
            # cosine distance to all vectors in the cluster
            descrs = np.vstack(group['descriptor'].values)
            distmat = cdist(descrs, descrs, metric='cosine')
            idx = np.argmin(distmat.mean(axis=0))
            selected_ref = group.iloc[idx].copy()
            label = selected_ref['label']
            if label in self.df_gallery['label']:
                selected_ref['label'] = f'{label}_clust'
            unkown_refs.append(selected_ref)

        return unkown_refs

    def filter_duprefs(self, df, std_margin=3, inplace=False):
        """Return a new gallery, filtering items that are too much alike.
        
        We want to avoid that an identity that was in the original gallery would
        have a second representation that is marked as unknown. Because, in that case,
        query faces of that known identity might later get classified as unknown.
        
        :param pd.DataFrame df: the gallery `DataFrame`
        :param int std_margin: the maximal number of standard deviations that two
        embeddings can be closer to each other than the mean distance between
        the gallery embeddings
        :param bool inplace: perform the filtering operation inplace
        """
        distmat = cdist(np.vstack(df['descriptor'].values),
                        np.vstack(df['descriptor'].values),
                        metric='cosine')

        # Mask diagonal and upper triagular
        mamat = ma(distmat, np.triu(np.ones(distmat.shape[0])))
        mean = mamat.mean()
        std = mamat.std()

        dup_refs = np.argwhere(mamat < (mean - std_margin*std))

        drop_idxs = []
        for dupref in dup_refs:
            df_dupref = df.iloc[dupref]

            known_labels = self.df_gallery['label']
            drop = df_dupref[df_dupref['label'].apply(lambda l:
                                                      l not in known_labels)]
            if len(drop) != 1:
                # We must delete exactly one,
                # so just drop the first one
                drop = df_dupref.iloc[[0]]
            drop_idxs.append(drop.index[0])
        return df.drop(index=drop_idxs, inplace=inplace)
