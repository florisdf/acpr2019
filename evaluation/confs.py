from sklearn.cluster import DBSCAN
from flutil.classifier.euclid import EuclideanClassifier
from flutil.detect.face.mtcnn import MTCNN
from flutil.extract.face.dlib import DlibFaceExtractor
from pathlib import Path

def keep_most_central(df):
    # We assume that each image is of size 250 x 250
    # To check that this is indeed the case, uncomment the code below
    # from PIL import Image
    # assert all(df['image'].apply(lambda d: Image.open(d).size == (250, 250)))
    
    df['dist'] = df['detection'].apply(lambda d: d.center.dist((125, 125)))
    return (df.sort_values(by='dist')
            .groupby('image')
            .first()
            .reset_index()
            .drop(columns='dist'))


def lfw_make_labeled(df):
    df = keep_most_central(df)
    df['label'] = df['image'].apply(lambda p: p.parent.name)
    return df
    
    
conf = {
    'data': {
        'data': [{'path': Path('../data/LFW/raw'),  # The location of the images
                  'name': 'LFW',  # The name to use to refer to this data in cache and output files
                  'n_known': N_KNOWN,  # The number of known identities, i.e. the initial number of items in the gallery
                  'make_labeled': lfw_make_labeled}  # How to convert a DataFrame with the images to a DataFrame with labels per image or face
                 for N_KNOWN in range(3, 151)],  # We want to run a trial with different numbers of known identities. In this case everything in a range from 3 up to 150
      'lazy': True,  # If lazy, the data will be loaded from a cache file
      'n_gal': 1,  # The number of instances per identity in the gallery
      'seed': [*range(30)]  # The seed to use for selecting the gallery items. We want to run multiple trials for each value of N_KNOWN. Therefore, we set this seed as a list.
    },
    'detect': {
        'class': MTCNN,  # The class of the detector
        'lazy': True,  # If lazy, the detections will be loaded from a cache file (recommended)
        'bs': 16  # The batch size to use for the detector
    },
    'extract': {
        'class': DlibFaceExtractor,  # The class of the feature extractor
        'lazy': True,  # If lazy, the descriptors will be loaded from a cache file (recommended)
        'bs': 32  # The batch size to use for the feature extractor
    },
    'clf': {
        'clf': {'class': EuclideanClassifier, 'kwargs': {}},  # The classifier to use for classifying query descriptors
    },
    'cluster': {
        'object': DBSCAN(eps=0.42, min_samples=5, metric='cosine'),  # The object that will perform the clustering
        'apply_ciga': [True, False]  # Whether or not to apply CIGA. By setting this to a list of True and False, each trial will be done once with CIGA and once without. That way, we can check the influence of adding CIGA.
    },
    'result': {
        # This lists a bunch of file names to output results to
        'mAP': Path('mAP'),
        'N_known': Path('N_known'),
        'N_label': Path('N_label'),
        'CIGA': Path('CIGA'),
        'dataset': Path('dataset'),
        'clf': Path('clf'),
        'extractor': Path('extractor'),
        'detector': Path('detector'),
    }
}